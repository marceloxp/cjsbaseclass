var cjsbaseclass_ns = cjsbaseclass_ns || {};
cjsbaseclass_ns.ui = (function($){
	'use strict';
	var self;

	return {
		init : function()
		{
			self = this;
		},

		isElement: function(o)
		{
			return (typeof HTMLElement === 'object' ? o instanceof HTMLElement : o && typeof o === 'object' && o !== null && o.nodeType === 1 && typeof o.nodeName==='string');
		},

		animateScroll: function(p_top, p_time, p_easing, p_callback)
		{
			var _top, _time, _easing;

			if (p_top instanceof jQuery)
			{
				_top = p_top[0].offsetTop;
			}
			else if (self.isElement(p_top))
			{
				_top = p_top.offsetTop;
			}
			else
			{
				_top = (p_top === undefined) ? 0 : p_top;
			}

			_time   = (p_time   === undefined) ? 500      : p_time;
			_easing = (p_easing === undefined) ? 'linear' : p_easing;
			$('html,body').stop().animate
			(
				{ 
					scrollTop: _top
				}, 
				_time,
				_easing,
				function() 
				{
					if ($(this).prop('tagName').toLowerCase() === 'body')
					{
						if (p_callback !== undefined)
						{
							p_callback();
						}
					}
				}
			);
		}
	};

})(window.cjsbaseclass_jquery);

cjsbaseclass_ns.ui.init();