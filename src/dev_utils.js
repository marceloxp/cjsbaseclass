var cjsbaseclass_ns = cjsbaseclass_ns || {};
cjsbaseclass_ns.dev = (function(){
	'use strict';
	var self;
	var cookie = cjsbaseclass_ns.cookie;

	return {
		init : function()
		{
			self = this;
			self.initVars();
		},

		initVars: function()
		{
			var k = cookie.get('cjsbaseclass_classes_talks');
			self.classes_talks = (empty(k)) ? [] : k.split(',');
		},

		/* BEGIN QUITE ALL CLASSES DEBUG */
		aloneOn: function(p_class_name)
		{
			if (self.hasAlone(p_class_name)){ return; }
			if (self.classes_talks.indexOf(p_class_name) === -1)
			{
				self.classes_talks.push(p_class_name.toLowerCase());
				cookie.set('cjsbaseclass_classes_talks', self.classes_talks.join(','), { expires: 1 });
			}
			window.location.reload();
		},

		anyAlone: function()
		{
			return (self.classes_talks.length > 0);
		},

		hasAlone: function(p_class_name)
		{
			return (self.classes_talks.indexOf(p_class_name.toLowerCase()) >= 0);
		},

		aloneOff: function()
		{
			self.classes_talks = [];
			cookie.set('cjsbaseclass_classes_talks', '', { expires: 1 });
			window.location.reload();
		},
		/* END QUITE ALL CLASSES DEBUG */

		/* BEGIN LOAD JAVSCRIPT IN RUNTIME */
		loadScript: function(p_src, p_callback)
		{
			// source: https://gist.github.com/hagenburger/500716
			var script = document.createElement('script'), loaded;
			script.setAttribute('src', p_src);
			if (p_callback !== undefined)
			{
				script.onreadystatechange = script.onload = function()
				{
					if (!loaded)
					{
						p_callback();
					}
					loaded = true;
				};
			}
			document.getElementsByTagName('head')[0].appendChild(script);
		},
		/* END LOAD JAVSCRIPT IN RUNTIME */

		/* BEGIN GLOBAL DEBUG */
		setGlobalDebug: function(p_arg)
		{
			if ( (p_arg === undefined) || (p_arg === true) )
			{
				cookie.set('cjsbaseclass_global_debug', '1', { expires: 1 });
			}
			else
			{
				cookie.remove('cjsbaseclass_global_debug');
			}
			window.location.reload();
		},

		getGlobalDebug: function()
		{
			return ((cookie.get('cjsbaseclass_global_debug') === '1') || false);
		}
		/* END GLOBAL DEBUG */

	};

})();

cjsbaseclass_ns.dev.init();