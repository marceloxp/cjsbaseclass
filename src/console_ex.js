if(!window.console)
{
	var console =
	{
		log     : function(){},
		warn    : function(){},
		error   : function(){},
		time    : function(){},
		timeEnd : function(){},
		apply   : function(){}
	};
}

function TConsoleEx()
{
	'use strict';
	var self;

	this.init = function(p_name)
	{
		self = this;
		self.initVars(p_name);
		self.processTriggers();
	};

	this.initVars = function(p_name)
	{
		self.debug = true;
		self.version = '1.0.0';
		self.name = (p_name === undefined) ? 'Untitled' : p_name;
		self.format = { };
		self.setColor('white');
		self.is = 
		{
			'chrome'  : is.chrome(),
			'firefox' : is.firefox(),
			'safari'  : is.safari(),
			'ie'      : is.ie(),
			'ie9'     : is.ie(9)
		};
		self.setActive(false);
	};

	this.processTriggers = function()
	{

	};

	this.setDebug = function(p_arg)
	{
		self.debug = p_arg;
	};

	this.setActive = function(p_arg)
	{
		self.active = p_arg;
		self.___refreshFmtCap();
	};

	this.setColor = function(p_color)
	{
		self.format.background = p_color;
		self.format.color = _u_invertRgb( (p_color.substring(0,1) === '#') ? _u_hexToRgb(p_color) : _u_hexToRgb( _u_colourNameToHex(p_color) ) );
	};

	this.___refreshFmtCap = function()
	{
		self.capfmt = ( (self.is.chrome || self.is.firefox) && (self.active) );
		self.capbro = (self.is.chrome || self.is.firefox);
	};

	this.___getFmtColorName = function(p_name)
	{
		var str_background = ((self.is.firefox) ? 'background-color' : false) || 'background';
		var fontweight = (this.format.color === 'black') ? 'normal' : 'bold';
		return [
			'%c' + p_name,
			'padding-left: 5px; padding-right: 5px; padding-top: 2px; padding-bottom: 2px; border: 1px solid black; font-weight: ' + fontweight + '; color:' + self.format.color + '; ' + str_background + ': ' + self.format.background
		];
	};

	this.___getStrMntArguments = function(p_args)
	{
		var mnt = [];
		for(var k = 0; k < p_args.length; k++)
		{
			mnt.push('arguments[' + k + ']');
		}
		return mnt.join(',');
	};

	this.___getEvalMntArguments = function(p_args)
	{
		if (self.debug === true)
		{
			self.___refreshFmtCap();
			if (self.capbro)
			{
				return 'console.log("' + self.name.toString() + '",' + self.___getStrMntArguments(p_args) + ');';
			}
			else
			{
				return 'console.log(' + self.___getStrMntArguments(p_args) + ');';
			}
		}
	};

	this.___isFmtCap = function()
	{
		self.___refreshFmtCap();
		return ( (self.capbro === true) && (self.active === true) );
	};

	this.log = function()
	{
		if (self.debug)
		{
			/*jslint evil: true */
			if (!self.___isFmtCap())
			{
				eval(self.___getEvalMntArguments(arguments));
			}
			else
			{
				var fmt_name = self.___getFmtColorName(self.name);
				var mntargs = self.___getStrMntArguments(arguments);
				var streval = 'console.log("' + fmt_name[0] + '", "' + fmt_name[1] + '", ' + mntargs + ');';
				eval(streval);
			}
		}
	};

	this.print = function()
	{
		if (self.debug)
		{
			/*jslint evil: true */
			if (!self.___isFmtCap())
			{
				eval(self.___getEvalMntArguments(arguments));
			}
			else
			{
				var fmt_name = self.___getFmtColorName(self.name);
				var mntargs = self.___getStrMntArguments(arguments);
				var streval = 'console.log("' + fmt_name[0] + '", "' + fmt_name[1] + '", ' + mntargs + ');';
				eval(streval);
			}
		}
	};

	this.info = function()
	{
		if (self.debug)
		{
			/*jslint evil: true */
			if (!self.___isFmtCap())
			{
				eval(self.___getEvalMntArguments(arguments));
			}
			else
			{
				var fmt_name = self.___getFmtColorName(self.name);
				var mntargs = self.___getStrMntArguments(arguments);
				var streval = 'console.info("' + fmt_name[0] + '", "' + fmt_name[1] + '", ' + mntargs + ');';
				eval(streval);
			}
		}
	};

	this.alert = function()
	{
		if (self.debug)
		{
			/*jslint evil: true */
			if (!self.___isFmtCap())
			{
				eval(self.___getEvalMntArguments(arguments));
			}
			else
			{
				var fmt_name = self.___getFmtColorName(self.name);
				var mntargs = self.___getStrMntArguments(arguments);
				var streval = 'console.warn("' + fmt_name[0] + '", "' + fmt_name[1] + '", ' + mntargs + ');';
				eval(streval);
			}
		}
	};

	this.danger = function()
	{
		if (self.debug)
		{
			/*jslint evil: true */
			if (!self.___isFmtCap())
			{
				eval(self.___getEvalMntArguments(arguments));
			}
			else
			{
				var fmt_name = self.___getFmtColorName(self.name);
				var mntargs = self.___getStrMntArguments(arguments);
				var streval = 'console.error("' + fmt_name[0] + '", "' + fmt_name[1] + '", ' + mntargs + ');';
				eval(streval);
			}
		}
	};

	this.trace = function()
	{
		if (self.debug)
		{
			/*jslint evil: true */
			if (!self.___isFmtCap())
			{
				eval(self.___getEvalMntArguments(arguments));
			}
			else
			{
				var fmt_name = self.___getFmtColorName(self.name);
				var mntargs = self.___getStrMntArguments(arguments);
				var streval = 'console.trace("' + fmt_name[0] + '", "' + fmt_name[1] + '", ' + mntargs + ');';
				eval(streval);
			}
		}
	};

}

// window.ObjTConsoleEx = new TConsoleEx();
// window.ObjTConsoleEx.init('TMyClass');
// window.ObjTConsoleEx.setActive(true);
// window.ObjTConsoleEx.setColor('yellow');
// window.ObjTConsoleEx.info('Message Info', 3, {'name': 'xp', 'age': 42});
// window.ObjTConsoleEx.alert('Message Alert', 3, {'name': 'xp', 'age': 42});
// window.ObjTConsoleEx.danger('Message Danger', 3, {'name': 'xp', 'age': 42});
// window.ObjTConsoleEx.trace('Message Trace', 3, {'name': 'xp', 'age': 42});
// window.ObjTConsoleEx.print('Message Print', 3, {'name': 'xp', 'age': 42});