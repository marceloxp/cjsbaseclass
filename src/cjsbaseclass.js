window.CjsBaseClass = function($, objname, options)
{
	'use strict';
	var self = this;
	self.version = '1.0.0';
	self.js_options = cjsbaseclass_ns.js_options;
	self.cookie = cjsbaseclass_ns.cookie;

	options          = (options === undefined) ? {} : options;
	self.options     = options;
	self.name        = objname;
	self.locks       = [];
	self.debug_mode  = (options.debug !== undefined) ? parseInt(options.debug) : CJS_DEBUG_MODE_0; // 0: none, 1: normal, 2: extended
	if (self.cookie.get('cjsbaseclass_global_debug') === '1')
	{
		self.debug_mode = CJS_DEBUG_MODE_2;
	}
	else
	{
		if (cjsbaseclass_ns.___cjsbtmp_silent_host !== undefined)
		{
			if (window.location.host === cjsbaseclass_ns.___cjsbtmp_silent_host)
			{
				self.debug_mode = CJS_DEBUG_MODE_0;
			}
		}
	}

	if (cjsbaseclass_ns.dev.anyAlone())
	{
		if (!cjsbaseclass_ns.dev.hasAlone(self.name))
		{
			self.debug_mode = CJS_DEBUG_MODE_0;
		}
	}
	self.debug       = (self.debug_mode > 0);
	self.highlighted = (options.highlighted !== undefined) ? options.highlighted : false;
	if (typeof self.highlighted === 'string')
	{
		if (self.highlighted.toLowerCase() === 'auto')
		{
			self.highlighted = cjsbaseclass_ns.colors.get();
		}
	}

	self.log = new TConsoleEx();
	self.log.init(self.name);
	self.log.setActive(self.debug);
	self.log.setDebug(self.debug);
	if (typeof self.highlighted === 'string')
	{
		self.log.setColor(self.highlighted);
		self.highlighted = true;
	}
	else if (self.highlighted === false)
	{
		self.log.setActive(false);
	}
	else if (self.highlighted === true)
	{
		self.log.setColor('yellow');
		self.highlighted = true;
	}

	self.is = 
	{
		'chrome'  : is.chrome(),
		'firefox' : is.firefox(),
		'safari'  : is.safari(),
		'ie'      : is.ie(),
		'ie9'     : is.ie(9)
	};
	self.browser = ((self.is.chrome) ? 'chrome' : false) || ((self.is.firefox) ? 'firefox' : false) || ((self.is.ie) ? 'ie' : false) || ((self.is.other) ? 'other' : false);

	this.___ensureClassTriggers = function()
	{
		if (window.___classes_triggers === undefined) { window.___classes_triggers = {}; }
	};

	this.___hasVarName = function(p_var_name)
	{
		return (window.___classes_triggers['__var_' + p_var_name] !== undefined);
	};

	this.setDebugMode = function(p_mode)
	{
		self.debug_mode = (p_mode);
		self.debug = (self.debug_mode > 0);
	};

	this.form = 
	{
		toJson: function(_form)
		{
			return cjsbaseclass_ns.form.toJson(_form);
		}
	};

	this.utils = 
	{
		aloneOn: function()
		{
			cjsbaseclass_ns.dev.aloneOn(self.name);
		},
		aloneOff: function()
		{
			cjsbaseclass_ns.dev.aloneOff();
		},
		loadScript: function(p_src, p_callback)
		{
			cjsbaseclass_ns.dev.loadScript(p_src, p_callback);
		},
		waitScrollTop: function(p_callback)
		{
			if (cjsbaseclass_ns.has_top_site === undefined)
			{
				cjsbaseclass_ns.has_top_site = false;
			}
			if (self.$_waitscrolltop_window === undefined)
			{
				self.$_waitscrolltop_window = $(window);
			}
			var y = self.$_waitscrolltop_window.scrollTop();
			if (y !== 0)
			{
				self.$_waitscrolltop_window.scrollTop(0);
				setTimeout(function() { self.utils.waitScrollTop(p_callback); }, 100 );
			}
			else
			{
				setTimeout
				(
					function()
					{
						if (!cjsbaseclass_ns.has_top_site)
						{
							cjsbaseclass_ns.has_top_site = true;
							self.trigger('site-scroll-on-top');
						}

						if (p_callback !== undefined)
						{
							p_callback();
						}
					},
					250
				);
			}
		}
	};

	this.ui = cjsbaseclass_ns.ui;

	this.triggerEvent = function(p_event_name)
	{
		self.trigger(self.name + '-on-' + p_event_name);
	};

	this.setDebug = function(p_args)
	{
		self.debug = p_args;
		self.debug_mode = (self.debug) ? 1 : 0;
	};

	this.waitReady = function(p_class_name, p_callback, p_miliseconds)
	{
		self.___waitTrigger(p_class_name + '-on-ready', p_callback, p_miliseconds, 'Class');
	};

	this.waitStarted = function(p_class_name, p_callback, p_miliseconds)
	{
		self.___waitTrigger(p_class_name + '-on-started', p_callback, p_miliseconds, 'Class');
	};

	this.waitTrigger = function(p_trigger_name, p_callback, p_miliseconds)
	{
		self.___waitTrigger(p_trigger_name, p_callback, p_miliseconds, 'Trigger');
	};

	this.___waitTrigger = function(p_trigger_name, p_callback, p_miliseconds, p_log_name)
	{
		self.___ensureClassTriggers();
		if (!self.___hasVarName(p_trigger_name))
		{
			var miliseconds = (p_miliseconds === undefined) ? 250 : p_miliseconds;
			setTimeout
			(
				function()
				{
					self.log.print('wait' + p_log_name + ': ' + p_trigger_name);
					self.___waitTrigger(p_trigger_name, p_callback, p_miliseconds, p_log_name);
				},
				miliseconds
			);
		}
		else
		{
			self.log.print('wait' + p_log_name + ': ' + p_trigger_name + ' triggered!');
			if (p_callback !== undefined)
			{
				p_callback();
			}
		}
	};

	this._onTrigger = function(p_trigger_name, p_callback)
	{
		self.log.print('Trigger Listener: ' + p_trigger_name);
		$(document).on
		(
			p_trigger_name,
			function(e, a)
			{
				if (self.debug_mode > 1)
				{
					if (a !== undefined)
					{
						self.log.print('Trigger Listened: ' + p_trigger_name, a);
					}
					else
					{
						self.log.print('Trigger Listened: ' + p_trigger_name);
					}
				}
				p_callback(a);
			}
		);
	};

	this.onTrigger = function()
	{
		var k = 0;
		while( k < arguments.length )
		{
			self._onTrigger(arguments[k], arguments[++k]);
			k++;
		}
	};

	this.trigger = function(p_trigger_name, p_args)
	{
		self.___ensureClassTriggers();
		if (!self.___hasVarName(p_trigger_name))
		{
			window.___classes_triggers['__var_' + p_trigger_name] = true;
		}

		if (p_args === undefined)
		{
			$(document).trigger(p_trigger_name);
			self.log.print('Trigger Fired: ' + p_trigger_name );
		}
		else
		{
			$(document).trigger(p_trigger_name, p_args);
			self.log.print('Trigger Fired: ' + p_trigger_name , p_args);
		}
	};

	this.lock = function(p_arg)
	{
		var tmp = p_arg.split(',');
		var name;
		for (var k = 0; k < tmp.length; k++)
		{
			name = tmp[k].trim();
			self.locks[name] = true;
		}
	};

	this.isLocked = function(p_arg)
	{
		var tmp = p_arg.split(',');
		var name;
		for (var k = 0; k < tmp.length; k++)
		{
			name = tmp[k].trim();
			if (self.locks[name] !== true)
			{
				return false;
			}
		}
		return true;
	};

	this.is_locked = function(p_arg)
	{
		return self.isLocked(p_arg);
	};

	this.islocked = function(p_arg)
	{
		return self.isLocked(p_arg);
	};

	this.unlock = function(p_arg)
	{
		var tmp = p_arg.split(',');
		var name;
		for (var k = 0; k < tmp.length; k++)
		{
			name = tmp[k].trim();
			self.locks[name] = false;
		}
	};

	/*
		// FUNCTION SNIPPET
		self.ajax
		(
			{
				'options':
				{
					slug     : 'slug-name',
					exclusive: false,
					autowait : true,
					url      : 'ajax/slug-action',
					type     : 'POST',
					dataType : 'json',
					data     :
					{
						
					}
				},
				'before': function()
				{

				},
				'done': function(p_response)
				{

				},
				'fail': function()
				{
					
				},
				'always': function()
				{

				},
				'exception': function()
				{

				}
			}
		);
	*/
	this.ajax = function()
	{
		try
		{
			var start = new Date().getTime();

			var args = arguments[0] || {};

			var onBefore    = (!empty(args.before   )) ? args.before    : function(){};
			var onDone      = (!empty(args.done     )) ? args.done      : function(){};
			var onFail      = (!empty(args.fail     )) ? args.fail      : function(){};
			var onAlways    = (!empty(args.always   )) ? args.always    : function(){};
			var onException = (!empty(args.exception)) ? args.exception : function(){};

			if (empty(args.options))
			{
				throw 'Invalid parameters!';
			}
			var options = args.options;
			if (options.slug === undefined)
			{
				throw 'Slug option is required!';
			}
			var slug = options.slug;
			delete options.slug;

			var has_autowait = false;
			var autowait = false;
			if (options.autowait !== undefined)
			{
				has_autowait = true;
				autowait = options.autowait;
				delete options.autowait;
			}

			var exclusive = (!empty(options.exclusive)) ? options.exclusive : false;
			delete options.exclusive;

			if (exclusive)
			{
				if (self.isLocked('lock-ajax-request-' + slug))
				{
					return false;
				}
			}

			if (autowait === true)
			{
				var style = '';
				var center_object = '';

				style          = 'style="position: fixed; top: 0; left: 0; width: 100%; height: 100%; background-color: #000; filter:alpha(opacity=50); -moz-opacity:0.5; -khtml-opacity: 0.5; opacity: 0.5; z-index: 10000;"';
				center_object  = '<table style="height:100%; width: 100%; margin: 0; padding: 0; border: 0;"><tr><td style="vertical-align: middle; text-align: center;">';
				center_object += '<img src="data:image/gif;base64,R0lGODlhMAAwAPYAAAAAAP///w4ODhwcHDIyMlBQUCwsLAQEBBISEkhISCQkJFZWVjo6OhYWFkBAQAgICDY2NqysrKioqBoaGnBwcN7e3u7u7v///76+vgoKCoyMjGxsbMrKypSUlIKCgi4uLvj4+IiIiFJSUqamptTU1Orq6pqamiAgIJ6enn5+foSEhHR0dD4+PrCwsERERKKionp6eubm5szMzGhoaLS0tExMTF5eXmRkZPT09OLi4sLCwri4uMbGxigoKFpaWtjY2HZ2dpaWltzc3GJiYpCQkNDQ0Lq6uvDw8AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAMAAwAAAH/4AAgoOEhYaHiImKi4yNjo0TLAQfj5WHBiIUlAAuK56DHywDlo8dIyMqggsRrIMUniKkiQgIgh4kuLUrFbyCEKwRNbKHCRQUGQAfF8spBynLF4ImvBXIAAkMwwC/rBqCJcsWACrQgiDLGIIMCwsOB8MS1BsAJtAGGuUi0CsAA+wFDrRNsAANwgloLeotA8ABWoYH/xIIsGTAwUQAC6CBOADtwoty0MQlWFCgwChBBh4wGlAywUkM0DCggNZw2QxoIQz8IyAIQYF2jNaRTEDgwIOOz5bBiFFBRgRo/ki6A6Dz30lFVUtaLNBxBQtDEDjQ+FlSwIMENv4xeMeoAdkCCf8U1OSpiABJBQrYkSygYBGCiwAeOPhXgEEItosaVEwrFXCiBNgGqKT6z0AlAYQtCxqwTjMhlnAhMxhwwG0CUgrgjmoglF3AQiwJQyZ61ZKCAXb1tkyA+HPrlnRJIWBcEq4DBZMTDRjMrral4gmOO27EuTdFBwamayM1IEHx73EJCSBAvnx5z7IM3FjPnv3rzd/jn9aWOn5x9AIMENDPnzx6UgLgJeCAtSiCQEXvyeIAAw1cpoADs5k0DEQ2pMWgIgcowECEPy3w3yOp6VWhh9pRBVlJ7CSQnQEFVlKaAd51uECF833WYQHZAYAAhLxZ0hkA+cXITnCEYNOgIAqciGPqJaAtIFFPMBbQIiIPbBgjAxompwheEJJVW4mf8VjSAALMNqUhB6xTQJVCZtMIjDE6oNKGJbFGWiEP3ObdAtkkueeTi3S5pIk/4eXdaTAyEKV+KI4igKAFMCIAXBd15102EPIJAAElRcmbAx2qdAAB3vXV1iCCHQrkng1yKmWmAjTw5yADfBhUjLVEGemmJQHQpWVRekhfjJplSperhM4HKjtnPtIdQD3tWSCyj45US5k/uSnLo5PpOgiyANBJV5K2DpOpZ+Am2asgWm4X2LItglvtAmC62w964FKVo72OCDDAkfwGLPDAigQCACH5BAkKAAAALAAAAAAwADAAAAf/gACCg4SFhoeIiYqLjI2OjRMsBB+PlYcDBAkIgi4rnoMfLAOWjwsLBQaCCxGsgxSeIqSJAg+CDDYLCYIrFb2CEKwRNbKHBgUOggK4BaMpF8+CJr0VGQAHMzbVsgOnCakApgUEACrPF4Igzxi7rC8TxA7dDQAGywca5gAi5ivg0xwHiD0ocMrBA2WnBpjIx8FchgHmLkCwZMCBAEHcCiRgAIBgAQYv8pmzACCHOQ2CDnzQpmhAAY2jADDopqDeqRHmZpgLgfMZSQA9VnhYEVDRzG4EAnpM0AAXAwYxKsiIYG5BxBMAVujYqsMGIwPhjglAcApVg1qFIHCgEXHDBBkR/398W9TAo8aaCxgUTYTjWYwbES9E2HsIwUVBD+KVZRCTUYgLOgL7YJRg4wC0YE/NbQQhIo6YA2ZuxviysuUDdXVZ2vEMBYAGR00hK+QyrGkCjSsd4CHOlO0EhAeF9l16nCwEuMpqdKAAbaIBihfktvRyuYLDj0IHr1TRAHZi4AckqE4+gQJCAgioX79+NMUb8OPHn02afHnwABTYJ79ZgAEC/wWonnuVCKDAgQgiuIkiCFREnywOMDDPIwY6YBozAi1gg1MTInKAAgxcSNACBDain28bkvjdIAZU9pIp3vi3oG4NtPiiKRuqRkhtml2EgIXAWSIaAP6NN6JxhWzUoewCLqJSiUsEJXBYg+PNiMgDIRrJAIjOKXKghR7ltqIh0DU5gACmWWnIATMVgKWReTnSopEGyWQkbAME94AC4hHEEZPj5TKmIWA6SU+gB46nS4sM2Pjfi6MIUGgBjAig0WHijceRhXES8JKNwDkwYi0HZFLAeYx0mJiiRAY6j6cF/JjAAgI0EKiOA5RolJGb2EgpALACAGYqNpIIHpOfCsKpccGCquyIamY33mwIBLpgsJLOugmafoInKWZGDhKsneIIwqSupHA617jI/gpAl/i9K+oCM46bLa3xPrfZuPR4ly+FA3T478AEF5xIIAAh+QQJCgAAACwAAAAAMAAwAAAH/4AAgoOEhYaHiImKi4yNjo0IDgYDj5WHAwQJCIIGNwUEgwYMm5aOCwsFBpyeoIKnqaWJAg+CDDYLCaufggO3BaSxhQYFDoICvpSduwC2uIIHMzYZwQOoCaoAr6DKra/YKxERLxPBDtYNAAa+B9wAvagC2RXzHAfBDwWoDg/HqAPtzXINuEDwAgRLBhzEc2eNAYB8BRi08wYgR0ENzz5MWzSgQIEElJhZU6AOFbd3BQS8KGhBUI8VHlbYU8TgVQIC9iAmaHCLQQMDCn7eclCg4IUTAFboWKrDBiMDr4gJQIAqVQNahQQoGFhwwwQZRn9gW9QA4keSCxjMTISDYIwbRv8vRFh7CMFCAA/MVWUQklGICzri+mCUIAFfrFBNVoJgFAelAw5WEFlgqOPHwnwPlM1laQdBFABqvBBioTSHyvmqFr7Zt9IBHkBaxC1IrnLNqDeDuZhNEAMLjnoXtHYd18IQuowGqA0GoGCQjcyDnWDhorr16mMBCCDAvXv37KU8kBhPnnwEQpY9qvfIOZgE3gRbDhJggED9+9zBW1IB/wKGRQgkVAxzDvhUiVYOrFbAcI88sIANPaGTyAEKMKBgavo5okBqD95iwF2EGFCYR6dcQx8wj2gmIomnQNjeIB15E08khSHHSE2q0JcAi60UYpiEACgwIiyPWIbLQgHuiOLgIQ9YuGNEFWK1iAIKJAhRayBekuCTAwiw2pKFHFBTAU0+mZYjIj65DzNPNpBZIQ9steOZQs6ZQJaHWEnkigtQuWMuIkq0Y30kUiKAngUwIsBHCw0wokMJnkmARysmAFlqtByQSQEKNAJkXn9qNyc6k/4SqQAN2AljhotY6NEmKyYKQKkAWKkKn6w2IiSlgkTaCq2V9poamI44SowgCMxJCq2HJrDAJl7m41AwhyL25CC0srmMkLmWEulY2e4qK17RwUnUs9h6ZMyp5SbyDyHZpvNhu48IMACQ9Oar776JBAIAIfkECQoAAAAsAAAAADAAMAAAB/+AAIKDhIWGh4iJiouMjY6NCA4GA4+VhwMECQiCBjcFBIMGDJuWjgsLBQacnqCCp6mliQIPggw2Cwmrn4IDtwWksYUGBQ6CAr6UnbsAtriDCQzBAAOoCaoAr6DKra/XDKcOB8EO1Q0ABr4H29O+AtOvxcEPBagOD8eoA+vNuQ+vCe4qGXAQkFoBaADoFWCwrhuABKgKUOJEa9GAAgcnfjuoAB2qbb1QCTCQTRACevEUfatGQJzCBA1uMWhgQAHNWw4QwBNH8tVERT0xEtSJ0UCDioQEKLgYcVaCW6gYiGPUQCFHklIXEUClQMGpiAoWIQgI4AG5iAx+LqLpACoxson/EkAbUDHoNUcCXsECcMDBCiILDF08KDftgaq5LCnICKDGCyEWInMQTC+i3AQE1FZa3OKC58+eJ1xaablVKRegQWNgYfHsAs2PDqS2MGSqowFZg30OkkGa7xMsXAgfLvwuAAEEkitXbryUBxLQo0ePQGgwxusYEweTkBq0haQGCIQfn7y5JRXdP2MQOzBlLBYsYCtS6uCyxGATiOjXQAGCogMKMGBfZeY5AkNkCFoghAb+GWKAXBidYs1IwDzyAAQRpHdBDpR1404kctnmyAwe2HCAD0WkRsIh0JgjiAIQ7uWICDrUKEEPfK2Ag2czLPKAgAlgxECASCmiwA2ggbDC1yAZ3CCiYPUFKZEAl1VoyAEbOZCaDL0x8qCU9jAjZQOGFfLAUkEuwEAGP6RWAyP1FcVJml0FmcuDDAUZXoSUhJCafEkdVBCE0dSnJgAEFGVnX5XRAsFnJTTiYllx5kIlPeYk+ouhAjSQZmIHlHBBl48IiNEmD2IkiKYAxKlKqgsU6AiMcrYKUSusppqYA5VZ+cgAQcaDQJqksCqAoZtcemgwx9Yl5SCsirkMjLLGYuhd0dJawCBF+kYpPcBEeyxEcHlbiD6ERHuOAeWaO98Ak7or77z0JhIIACH5BAkKAAAALAAAAAAwADAAAAf/gACCg4SFhoeIiYqLjI2OjQgOBgOPlYcDBAkIggY3BQSDBgyblo4LCwUGnJ6ggqeppYkCD4IMNgsJq5+CA7cFpLGFBgUOggK+lJ27ALa4gwkMwQADqAmqAK+gyq2v1wynDgfBDtUNAAa+B9vTvgLTr8XBDwWoDg/HqAPrzbkPrwnuKhlwEJBaAWgA6BVgsK4bgASoClDiRGvRgAIHJ347qAAdqm29UAkwkE0QAnrxFH2rRkCcwgQNbjFoYEABzVsOEMATR/LVREU9MRLUidFAg4qEBCi4GHFWgluoGIhj1EAhR5JSFxFApUDBqYgKFiEICOABuYgMfi6i6QAqMbKJ/xJAG1Ax6DVHAl7B4vXt7qCLB+WmPVA1lyUFGQE0WAnOENOIchMQUFtp6davGOVOLTSAceZWpRC4zexAAVJEA84uoFwJ48HScBt13lxqoIHY0koNSOC6d4KwgwQQGE6cuN/aN5IrV55yWu/nhoMhfu7a70gCBrBrx55badfv34EhQjCweSkWLFgrUuogssRgE4jI10ABgqIDChi4p7fg+CMYFgQooBAa2GeIAXJhdIo1I4nnyAMQRHDBhBROmINj/KXiTiSaWTKDBzYc4EMRFV5AwiHQmCOIAgnu5YgIOsQoQQ8AHLACDhPOsMgD+vG2UH6nJYJOhSCsMEgGN9DmWPF7Pg4gQGQOFvKADStQ4ECJMmTQCII+2sOMj4sNoGQGH9QQwZkqZPBDiTUw0l5RnPC2QFe85YIgA0OssEINGFTgpw0AhFCiekkdVFCC0bS3QDQEYKTCmR30UOEJAEBAYQmNqFjWm7k8SY85jRbgg58VQAADhTEIckAJF2hZiX4YbYIgRoKEmgGFKACQA67SsAgnAIq2EioAJE4IAAIVthnLbsSYJCcpw1JAoQgADEEhDtII4OU5Pg4y7AMUnggACRfEEKQ0it41LAAWUDiVsrkNYhY9wKy7AoU+xJuIPoSse8CEKiiprywDaDrwwQgnnEggACH5BAkKAAAALAAAAAAwADAAAAf/gACCg4SFhoeIiYqLjI2OjQgOBgOPlYcDBAkIggY3BQSDBgyblo4LCwUGnJ6ggqeppYkCD4IMNgsJq5+CA7cFpLGFBgUOggK+lJ27ALa4gwkMwQADqAmqAK+gyq2v1wynDgfBDtUNAAa+B9vTvgLTr8XBDwWoDg/HqAPrzbkPrwnuKhlwEJBaAWgA6BVgsK4bgASoClDiRGvRgAIHJ347qAAdqm29UAkwkE0QAnrxFH2rRkCcwgQNbjFoYEABzVsOEMATR/LVREU9MRLUidFAg4qEBCi4GHFWgluoGIhj1EAhR5JSFxFApUDBqYgKFiEICOABuYgMfi6i6QAqMbKJ/xJAG1Ax6DVHAl7B4vXt7qCLB+WmPVA1lyUFGQE0WAnOENOIchMQUFtp6davGOVOLTSAceZWpRC4zexAAVJEA84uoFwJ48HScBt13lxqoIHY0koNSOC6d4KwgwQQGE6cuN/aN5IrV55yWu/nhoMhfu7a70gCBrBrx55badfv34EhQjCweSkHMyspdRBZorwFNmSaS3RAAYP29BYcf4T4a3z9uJ0jF0anWDOSeI4QZgBv+cFnQ3R/5ZeKO5FoZklfAIzE4CmgEQLNfAAoMOBejgCGS0Dk8YagIQ/cxyAD9p2WSE3sKaRWgISkNuIAAkS2IiEP2LACBQjcR2A0jSzIoPg9zDA4AAsGyJjBBzVEYKUKIQ54IiM17rUgPYitMGSRDLAwhJg1YFDBmjZk2GUBjAhwUEEDRvPClS6IOYMGVnbQwwWAXnACAAdkUgBwaw1iFm+5OLBmBSIM0acPj0IAQ6Ax/LUfI0b+AsALgR6gwpo7ZBAoCgDkcKo0IhYlSKAyAGACoD8AUESgACAQ6AU1BLMbMYL4EOgMAEQAaAkAUBCoCAAMESgO0gjAJAA/hAqAEbg+ECgJgpBwQQwyBnNAoMgCwAGuAFhgLQC95kbIB4FSIEi1gAqyQqA+uDseChdMRe8Fgox7gQq06ZuIAyhIAIPBDDfsMCOBAAAh+QQJCgAAACwAAAAAMAAwAAAH/4AAgoOEhYaHiImKi4yNjo0IDgYDj5WHAwQJCIIGNwUEgwYMm5aOCwsFBpyeoIKnqaWJAg+CDDYLCaufggO3BaSxhQYFDoICvpSduwC2uIMJDMEAA6gJqgCvoMqtr9cMpw4HwQ7VDQAGvgfb074C06/FwQ8FqA4Px6gD6825D68J7ioZcBCQWgFoAOgVYLCuG4AEqApQ4kRr0YACByd+O6gAHaptvVAJMJBNEAJ68RR9q0ZAnMIEDW4xaGBAAc1bDhDAE0fy1URFPTES1InRQIOKhAQouBhxVoJbqBiIY9RAIUeSUhcRQKVAwamIChYhCAjgAbmIDH4uoukAKjGyif8SQBtQMeg1RwJeweL17e6giwflpj1QNZclBRkBNFgJzhDTiHITEFBbaenWrxjlTi00gHHmVqUQuM3sQAFSRAPOLqBcCePB0nAbdd5caqCB2NJKDUjguneCsIMEEBhOnLjf2jeSK1eeclrv54aDIX7u2u9IAgawa8eeW2nX79+BIUIwsHkpBzMrKXUQWaK8BTZkmkt0QAGD9vQWHH+E+Gt8/bidIxdGp1iDAAEnlEKYAbzlB58N0f2VXyru9LCCB0CI10hfAIzU4CmgEQLNfAdQoMOJOoRQCWC4BEQebxoa0gMHF9R4gQwFDDFBI12xp5BaARKygo01BvFBBBGMAIH+Igds9MB9BEbDyAFFEHmBAxnIUMGWNbBgwGllLcXbAtEoMGCLizxAZAkZAECDjSgUsMIKFCDAAAMsZJIKAQRSIoCPGDHiQ5GCDFmjBQe8gKQKLsw5A4MHHeBAfrQcoCdwi8DQigFEDrRlBSIMgWQHkUAkQANjRqePJQcQGQQAL9h4gApb7gCAj6pAqp80RhApiI0yAGBCjT8IeJAgk54SoyNv1rjJoDXOAEAENZbQIXsLbCLAmFLGMsMFPgjyg6wA9FpjLby1YuZ+wbRarSA0nguAmYEKAmZuH9hIgbg2GoNtkLkBgAAKF0w1rrzn3BbwIw6gIAEMC0cs8cSVBAIAIfkECQoAAAAsAAAAADAAMAAAB/+AAIKDhIWGh4iJiouMjY6NCA4GA4+VhwMECQiCBjcFBIMGDJuWjgsLBQacnqCCp6mliQIPggw2Cwmrn4IDtwWksYUGBQ6CAr6UnbsAtriDCQzBAAOoCaoAr6DKra/XDKcOB8EO1Q0ABr4H29O+AtOvxcEPBagOD8eoA+vNuQ+vCe4qGXAQkFoBaADoFWCwrhuABKgKUOJEa9GAAgcnfjuoAB2qbb1QCTCQTRACevEUfatGQJzCBA1uMWhgQAHNWw4QwBNH8tVERT0xEtSJ0UCDioQEKLgYcVaCW6gYiGPUQCFHklIXEUClQMGpiAoWIQgI4AG5iAx+LqLpACoxson/EkAbUDHoNUcCXsHi9e3uoIsH5aY9UDWXJQUZATRYCc4Q04hyExBQW2np1q8Y5U4tNIBx5lalELjN7EABUkQDzi6gXAnjwdJwG3XeXGqggdiG1BnYzXv3CWnTErgeniDsIBYkkitXbgR4pxvQo0NPCcDGhevYswNHPHy43x4ywosX3wK40q7o0QM7dADCCiJEMkhzMLOSgw5CLFSwYGGFvAU2yGROIhOgAEJ22O0QDGJfBbjAbYeQgOB1RQxBwG+WEGaAcPScEqBhhACRHQ4rHNDDCh4AsV4jfQEwEoengDbIAdcFIc4BFOigow4hVAIYLgEhsOEvi/TAQXYyFDDE6QSNdOWAcPkktcgKCAbxQQQRjACBIgds9AADMEbDyAFFIOhABjJUoKYLgxXywFJQRqOAXPQAtMgD2ZUgHw3YoTDnQedAM6QBBGC0motPusaIDzUKQuV1Fhzw5ALRFJqKcAlI2iEtB2RSgHGLwNCKAdkZgACU5lgaCUQCNAAliPpkmF0QgWIkiKUAJKrKkA9KY4R2uULUCq5DGuZAhys+wud1m5xKDym4CjDpJgLEKc0MF/jACYeD4MqMcK3M6RdwwfZ6q60A/FnAIKeRa9az3aIrLavkJhJrvOtyAmG9eA0wIL8AByywIoEAACH5BAkKAAAALAAAAAAwADAAAAf/gACCg4SFhoeIiYqLjI2OjQgOBgOPlYcDBAkIggY3BQSDBgyblo4LCwUGnJ6ggqeppYkCD4IMNgsJq5+CA7cFpLGFBgUOggK+lJ27ALa4gwkMwQADqAmqAK+gyq2v1wynDgfBDtUNAAa+B9vTvgLTr8XBDwWoDg/HqAPrzbkPrwnuKomg0INXtWj0CjBY1w1AAlQFKHGitcgFjYszalVTgA7Vtl6oBBjIJggBvXiKXhxZeUTHgAMJEzS4xaCBAQU2bzlAAE/cyFcSEx1AcaFo0SE8C6RqQJGQAAUDEhaYleAWKgbiGIkwWhTDN6yLCKBSoOAURAWLBgCbgIPrhaCL/2w6sEosoFANRDTYuCaBayUBr2Dx+naNUAELiC1UkKHCwNYLEywpKJCAUoNvZlEOmuHWqAUNcB9BFWtWaYIEWQkduEGi84VgCOiadqCg6aEHHtoWpSFNKWXadhtlMHEBgjQDkoIfUmegufPmJ6RNS+C7egK0g1iQ2M6duxHpnW6IHy9esw3XRqVPrl69MIAeMuLLl99C+lOy+PEDWw5hBREiGUjjQE2VONCBEIohtoI8C9hAkzmJTIACCK7tEMxkZjm4gAHKCdJaZ0X4YACElRxgE3X0nOJgLoUAwRUOKxwQyWmpOUIYACKheEorqhUVRFYKnKaUe4xERQ9AJRlA3eJ+tzEgpEIKMGBbImQ5QF0+TqVlJYoDCHCaJooc8E0BDzip1ALRNKIkivYwg+JlLxXyAFRXRhPklUguYuWQnFxJFnW5KLkQdQYQcCYlAuypFCMCUBbQAEJGYyWaABiaCqAHOJAiLQdkUgB2cQ3ywJ65eEmPOZbOuIAADeA5iD6lmPnLOSgKYikAe6qiJD1EVhIkn7g+1Mqtu7Ko6SlMPgIpMSVdScqtiT60iamUBpMoRbsWMMitbi4TZK8CPlQYt79qK8iU0o1KDzDcRruqdIrAuu2inHAI718DkHjvvvz220ggADsAAAAAAAAAAAA8YnIgLz4KPGI+V2FybmluZzwvYj46ICBteXNxbF9xdWVyeSgpIFs8YSBocmVmPSdmdW5jdGlvbi5teXNxbC1xdWVyeSc+ZnVuY3Rpb24ubXlzcWwtcXVlcnk8L2E+XTogQ2FuJ3QgY29ubmVjdCB0byBsb2NhbCBNeVNRTCBzZXJ2ZXIgdGhyb3VnaCBzb2NrZXQgJy92YXIvcnVuL215c3FsZC9teXNxbGQuc29jaycgKDIpIGluIDxiPi9ob21lL2FqYXhsb2FkL3d3dy9saWJyYWlyaWVzL2NsYXNzLm15c3FsLnBocDwvYj4gb24gbGluZSA8Yj42ODwvYj48YnIgLz4KPGJyIC8+CjxiPldhcm5pbmc8L2I+OiAgbXlzcWxfcXVlcnkoKSBbPGEgaHJlZj0nZnVuY3Rpb24ubXlzcWwtcXVlcnknPmZ1bmN0aW9uLm15c3FsLXF1ZXJ5PC9hPl06IEEgbGluayB0byB0aGUgc2VydmVyIGNvdWxkIG5vdCBiZSBlc3RhYmxpc2hlZCBpbiA8Yj4vaG9tZS9hamF4bG9hZC93d3cvbGlicmFpcmllcy9jbGFzcy5teXNxbC5waHA8L2I+IG9uIGxpbmUgPGI+Njg8L2I+PGJyIC8+CjxiciAvPgo8Yj5XYXJuaW5nPC9iPjogIG15c3FsX3F1ZXJ5KCkgWzxhIGhyZWY9J2Z1bmN0aW9uLm15c3FsLXF1ZXJ5Jz5mdW5jdGlvbi5teXNxbC1xdWVyeTwvYT5dOiBDYW4ndCBjb25uZWN0IHRvIGxvY2FsIE15U1FMIHNlcnZlciB0aHJvdWdoIHNvY2tldCAnL3Zhci9ydW4vbXlzcWxkL215c3FsZC5zb2NrJyAoMikgaW4gPGI+L2hvbWUvYWpheGxvYWQvd3d3L2xpYnJhaXJpZXMvY2xhc3MubXlzcWwucGhwPC9iPiBvbiBsaW5lIDxiPjY4PC9iPjxiciAvPgo8YnIgLz4KPGI+V2FybmluZzwvYj46ICBteXNxbF9xdWVyeSgpIFs8YSBocmVmPSdmdW5jdGlvbi5teXNxbC1xdWVyeSc+ZnVuY3Rpb24ubXlzcWwtcXVlcnk8L2E+XTogQSBsaW5rIHRvIHRoZSBzZXJ2ZXIgY291bGQgbm90IGJlIGVzdGFibGlzaGVkIGluIDxiPi9ob21lL2FqYXhsb2FkL3d3dy9saWJyYWlyaWVzL2NsYXNzLm15c3FsLnBocDwvYj4gb24gbGluZSA8Yj42ODwvYj48YnIgLz4KPGJyIC8+CjxiPldhcm5pbmc8L2I+OiAgbXlzcWxfcXVlcnkoKSBbPGEgaHJlZj0nZnVuY3Rpb24ubXlzcWwtcXVlcnknPmZ1bmN0aW9uLm15c3FsLXF1ZXJ5PC9hPl06IENhbid0IGNvbm5lY3QgdG8gbG9jYWwgTXlTUUwgc2VydmVyIHRocm91Z2ggc29ja2V0ICcvdmFyL3J1bi9teXNxbGQvbXlzcWxkLnNvY2snICgyKSBpbiA8Yj4vaG9tZS9hamF4bG9hZC93d3cvbGlicmFpcmllcy9jbGFzcy5teXNxbC5waHA8L2I+IG9uIGxpbmUgPGI+Njg8L2I+PGJyIC8+CjxiciAvPgo8Yj5XYXJuaW5nPC9iPjogIG15c3FsX3F1ZXJ5KCkgWzxhIGhyZWY9J2Z1bmN0aW9uLm15c3FsLXF1ZXJ5Jz5mdW5jdGlvbi5teXNxbC1xdWVyeTwvYT5dOiBBIGxpbmsgdG8gdGhlIHNlcnZlciBjb3VsZCBub3QgYmUgZXN0YWJsaXNoZWQgaW4gPGI+L2hvbWUvYWpheGxvYWQvd3d3L2xpYnJhaXJpZXMvY2xhc3MubXlzcWwucGhwPC9iPiBvbiBsaW5lIDxiPjY4PC9iPjxiciAvPgo=">';
				center_object += '</td></tr></table>';

				var div_id = sprintf('cjs-ajax-wait-%s', slug);
				var div    = sprintf('<div id="%s" %s class="cjs-ajax-wait">%s</div>', div_id, style, center_object);
				self.trigger('on-ajax-' + slug + '-overlay-before');

				$('body').append(div);

				self.trigger('on-ajax-' + slug + '-overlay-show');
			}

			self.trigger('on-ajax-' + slug + '-before');
			onBefore();
			$.ajax
			(
				options
			)
			.done(function(p_response)
			{
				self.trigger('on-ajax-' + slug + '-done', p_response);
				onDone(p_response);
			})
			.fail(function(p_response) {
				self.trigger('on-ajax-' + slug + '-fail', p_response);
				onFail();
			})
			.always(function()
			{
				var end = new Date().getTime();
				var elapsed = (end - start);

				if (has_autowait)
				{
					var delay = (elapsed < 1000) ? 1000 : 0;
					setTimeout
					(
						function()
						{
							self.trigger('on-ajax-' + slug + '-overlay-before-end');
							$('#' + div_id).remove();
							self.trigger('on-ajax-' + slug + '-overlay-after-end');
						},
						delay
					);
				}
				self.trigger('on-ajax-' + slug + '-always', { 'milliseconds_elapsed': elapsed });
				if (exclusive)
				{
					self.unlock('lock-ajax-request-' + slug);
				}
				onAlways();
			});
		}
		catch(err)
		{
			if (empty(slug))
			{
				throw err;
			}
			else
			{
				self.trigger('on-ajax-' + slug + '-exception', err);
				onException();
			}
		}
	};

	/*
		self.onAjaxTriggers
		(
			'slug-name',
			{
				'before': function()
				{

				},
				'done': function(p_response)
				{

				},
				'fail': function()
				{

				},
				'always': function()
				{

				},
				'exception': function()
				{

				}
			}
		);
	*/

	this.onAjaxTriggers = function(p_slug, p_triggers)
	{
		for(var key in p_triggers)
		{
			self.onTrigger
			(
				'on-ajax-' + p_slug + '-' + key,
				p_triggers[key]
			);
		}
	};

	this.events = 
	{
		'onCreate': function()
		{
			self.triggerEvent('after-create');
			if (typeof self.processTriggers  === 'function') { self.processTriggers(); }
			if (typeof self.onElementsEvents === 'function') { self.onElementsEvents(); }
			self.onReady();
		},
		'onReady': function()
		{
			self.triggerEvent('ready');
			self.start();
		},
		'onStarted': function()
		{
			self.triggerEvent('started');
			if (typeof self.execute === 'function')
			{
				self.triggerEvent('execute');
				self.execute();
			}
		}
	};

	if (self.debug_mode === 2)
	{
		self.log.print( { 'lib version': self.version, 'jquery': $.fn.jquery, 'js_options': self.js_options } );
	}

	window.cjsbaseclass_jquery(document).ready
	(
		function()
		{
			if (typeof self.init === 'function')
			{
				throw 'CjsBaseClass is running on newer version (greater than 1.4.0), please ajust your script (' + self.name + ') to support this version.';
			}
			if (typeof self.initialize === 'function')
			{
				throw 'CjsBaseClass is running on newer version (greater than 2.0.0), please ajust your script (' + self.name + ') to support this version.';
			}
			if (typeof self.create === 'function')
			{
				self.trigger(self.name + '-on-before-create');
				self.create();
			}
		}
	);
};