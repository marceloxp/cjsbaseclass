var cjsbaseclass_ns = cjsbaseclass_ns || {};
cjsbaseclass_ns.js_options = {};
cjsbaseclass_ns.scripts = document.getElementsByTagName('script');
cjsbaseclass_ns.current_script = null;

for(var k in cjsbaseclass_ns.scripts)
{
	var source = cjsbaseclass_ns.scripts[k].src;
	if (typeof source === 'string')
	{
		if (source.indexOf('cjsbaseclass.') >= 0)
		{
			cjsbaseclass_ns.current_script = cjsbaseclass_ns.scripts[k];
			break;
		}
	}
}
if (cjsbaseclass_ns.current_script === null)
{
	throw 'Get current script fail routine!';
}
cjsbaseclass_ns.script_name = cjsbaseclass_ns.current_script.src.split('/').pop();
if ( (cjsbaseclass_ns.script_name.indexOf('cjsbaseclass.js') >= 0) || (cjsbaseclass_ns.script_name.indexOf('cjsbaseclass.min.js') >= 0) )
{
	for(var k = 0; k < cjsbaseclass_ns.current_script.attributes.length; k++)
	{
		var attr = cjsbaseclass_ns.current_script.attributes[k];
		if (attr.name.substring(0,4) === 'data')
		{
			var data_name = attr.name.substr(5);
			var data_value = attr.value;
			cjsbaseclass_ns.js_options[data_name] = data_value;
		}
	}
}
cjsbaseclass_ns.___cjsbtmp_removeall = (cjsbaseclass_ns.js_options['jquery-exclusive'] === 'true');
window.cjsbaseclass_jquery = jQuery.noConflict(cjsbaseclass_ns.___cjsbtmp_removeall);
if (!cjsbaseclass_ns.___cjsbtmp_removeall)
{
	window.jQuery = window.$ = jQuery;
}

delete cjsbaseclass_ns.scripts;
delete cjsbaseclass_ns.current_script;
delete cjsbaseclass_ns.script_name;

if (cjsbaseclass_ns.js_options['silent-host'] !== undefined)
{
	cjsbaseclass_ns.___cjsbtmp_silent_host = cjsbaseclass_ns.js_options['silent-host'];
}