var cjsbaseclass_ns = cjsbaseclass_ns || {};
cjsbaseclass_ns.form = (function(){
	'use strict';
	var self;

	return {
		init : function()
		{
			self = this;
			self.initVars();
		},

		initVars: function()
		{

		},

		toJson: function(_form)
		{
			var $form = (typeof _form === 'string') ? $(_form) : _form;

			var unindexed_array = $form.serializeArray();
			var indexed_array = {};

			$.map
			(
				unindexed_array,
				function(n, i)
				{
					if (indexed_array[n.name] === undefined)
					{
						indexed_array[n.name] = n.value;
					}
					else
					{
						if (typeof indexed_array[n.name] === 'object')
						{
							indexed_array[n.name].push(n.value);
						}
						else
						{
							indexed_array[n.name] = [indexed_array[n.name], n.value];
						}
					}
				}
			);
			return indexed_array;
		}
	};

})();

cjsbaseclass_ns.form.init();