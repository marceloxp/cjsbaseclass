/* global CjsBaseClass,CJS_DEBUG_MODE_0,CJS_DEBUG_MODE_1,CJS_DEBUG_MODE_2 */
var ${1:namespace} = ${1:namespace} || {};
${1:namespace}.T${2:object_name} = function($, objname, options)
{
	'use strict';
	var self = this;

	this.initialize = function()
	{
		self.initVars();
		self.processTriggers();
		self.onElementsEvents();
	};

	this.initVars = function()
	{
		
	};

	this.onReady = function()
	{
		// CODE ON APLICATION IS READY
		self.start();
	};

	this.start = function()
	{
		// CODE ON APLICATION IS STARTED
		self.triggerStarted();
	};

	this.processTriggers = function()
	{

	};

	this.onElementsEvents = function()
	{

	};

	CjsBaseClass.call(this, $, objname, options);
};

${1:namespace}.${2:object_name} = new ${1:namespace}.T${2:object_name}
(
	window.cjsbaseclass_jquery,
	'${2:object_name}',
	{
		'debug'       : CJS_DEBUG_MODE_1,
		'highlighted' : 'auto',
		'another_opt' : 'custom_value'
	}
);