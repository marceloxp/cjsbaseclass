(function () {
   'use strict';

	var fs      = require('fs');
	var gulp    = require('gulp');
	var uglify  = require('gulp-uglify');
	var concat  = require('gulp-concat');
	var edit    = require('gulp-edit');
	var debug   = require('gulp-debug');
	var replace = require('gulp-replace');
	var rename  = require('gulp-rename');
	var bump    = require('gulp-bump');

	var getPackageJson = function () { return JSON.parse(fs.readFileSync('./package.json', 'utf8')); };
	var pkg = getPackageJson();
	var conf = 
	{
		'version' : pkg.version,
		'name'    : 'basejsclass'
	};

	gulp.task
	(
		'inc-patch',
		function()
		{
			gulp.src('./package.json')
				.pipe(bump())
				.pipe(gulp.dest('./'))
			;
			gulp.src('./bower.json')
				.pipe(bump())
				.pipe(gulp.dest('./'))
			;
		}
	);

	gulp.task
	(
		'inc-minor',
		function()
		{
			gulp.src('./package.json')
				.pipe(bump({type:'minor'}))
				.pipe(gulp.dest('./'))
			;
			gulp.src('./bower.json')
				.pipe(bump({type:'minor'}))
				.pipe(gulp.dest('./'))
			;
		}
	);

	gulp.task
	(
		'inc-major',
		function()
		{
			gulp.src('./package.json')
				.pipe(bump({type:'major'}))
				.pipe(gulp.dest('./'))
			;
			gulp.src('./bower.json')
				.pipe(bump({type:'major'}))
				.pipe(gulp.dest('./'))
			;
		}
	);

	// gulp.task
	// (
	// 	'inc-custom',
	// 	function()
	// 	{
	// 		gulp.src('./package.json')
	// 			.pipe(bump({version: '1.2.7'}))
	// 			.pipe(gulp.dest('./'))
	// 		;
	// 		gulp.src('./bower.json')
	// 			.pipe(bump({version: '1.2.7'}))
	// 			.pipe(gulp.dest('./'))
	// 		;
	// 	}
	// );

	gulp.task
	(
		'default',
		function()
		{
			gulp
				.src
				(
					[
						'bower_components/js-cookie/src/js.cookie.js',
						'bower_components/url.min/index.js',
						'src/console_ex.js',
						'bower_components/is_js/is.js',
						'src/jquery-2.2.4.min.js',
						'src/form_utils.js',
						'src/php.js',
						'src/inc_utils.js',
						'src/dev_utils.js',
						'src/jquery_cjs_middle.js',
						'src/jquery_cjs_before.js',
						'src/ui.js',
						'src/jquery.easing.js',
						'src/cjsbaseclass.js'
					]
				)
				.pipe(debug())
				.pipe(concat('cjsbaseclass.js'))
				.pipe(replace("self.version = '1.0.0';", "self.version = '" + conf.version + "';"))
				.pipe(edit(function(src, cb) {src = '/* (https://bitbucket.org/marceloxp/cjsbaseclass) Version: ' + conf.version + ' Last modified: ' + new Date().toLocaleString() + ' */ \n/* exported CjsBaseClass */ \n' + src; cb(null, src);}))
				.pipe(gulp.dest('dist/'))
				.pipe(uglify())
				.pipe(edit(function(src, cb) {src = '/* (https://bitbucket.org/marceloxp/cjsbaseclass) Version: ' + conf.version + ' Last modified: ' + new Date().toLocaleString() + ' */ \n/* exported CjsBaseClass */ \n' + src; cb(null, src);}))
				.pipe(rename({ suffix: '.min'}))
				.pipe(gulp.dest('dist/'))
				.on('end', function() { console.log('Gulp Task', 'Dist task complete'); })
			;

			gulp
				.src('src/main.js')
				.pipe(debug())
				.pipe(replace('${1:namespace}', 'MyNameSpace'))
				.pipe(replace('${2:object_name}', 'myclass'))
				.pipe(gulp.dest('demo/cjsbaseclass/'))
				.pipe(gulp.dest('dist/'))
				.on('end', function() { console.log('Gulp Task', 'Sublime Main Demo task complete'); })
			;

			gulp
				.src('src/main.js')
				.pipe(debug())
				.pipe
				(
					edit
					(
						function(src, cb)
						{
							src =
								'<snippet>\n\t<tabTrigger>class_instanced_main</tabTrigger>\n\t<scope>source.js</scope>\n\t<description>2x Base CjsBaseClass</description>\n\t<content>\n\t<![CDATA[\n' +
								src +
								']]>\n</content>\n</snippet>'
							;
							cb(null, src);
						}
					)
				)
				.pipe(replace('$', '\\$'))
				.pipe(replace('\\${', '${'))
				.pipe(rename('base_js_main_class.sublime-snippet'))
				.pipe(gulp.dest('dist/'))
				.on('end', function() { console.log('Gulp Task', 'Sublime Main Snippet task complete'); })
			;

			gulp
				.src('src/main-1x.js')
				.pipe(debug())
				.pipe
				(
					edit
					(
						function(src, cb)
						{
							src =
								'<snippet>\n\t<tabTrigger>class_instanced_main</tabTrigger>\n\t<scope>source.js</scope>\n\t<description>1x Base CjsBaseClass</description>\n\t<content>\n\t<![CDATA[\n' +
								src +
								']]>\n</content>\n</snippet>'
							;
							cb(null, src);
						}
					)
				)
				.pipe(replace('$', '\\$'))
				.pipe(replace('\\${', '${'))
				.pipe(rename('base_js_main_class-1x.sublime-snippet'))
				.pipe(gulp.dest('dist/'))
				.on('end', function() { console.log('Gulp Task', 'Sublime Main Snippet task complete'); })
			;
		}
	);

}());