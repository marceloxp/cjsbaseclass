var fs = require('fs');
var origin = 'bower_components/cjsbaseclass/dist/';
var target = process.env.APPDATA + '/Sublime Text 3/Packages/User/';
fs.writeFileSync(target + 'base_js_main_class.sublime-snippet', fs.readFileSync(origin + 'base_js_main_class.sublime-snippet'));
fs.writeFileSync(target + 'base_js_main_class-1x.sublime-snippet', fs.readFileSync(origin + 'base_js_main_class-1x.sublime-snippet'));
fs.writeFileSync(target + 'cjsbaseclass.sublime-completions'  , fs.readFileSync(origin + 'cjsbaseclass.sublime-completions'));