var MyNameSpace = MyNameSpace || {};
MyNameSpace.Ttriggers = function($, objname, options)
{
	'use strict';
	var self = this;

	this.create = function()
	{
		self.events.onCreate();
	};

	this.onReady = function()
	{
		self.events.onReady();
	};

	this.start = function()
	{
		self.events.onStarted();
	};

	this.processTriggers = function()
	{
		self.onTrigger
		(
			'on-ajax-simple-before',
			function(p_args)
			{
				self.log.info('GET SIMPLE BEFORE');
			},
			
			'on-ajax-simple-always',
			function(p_args)
			{
				self.log.info('GET SIMPLE ALWAYS');
			},

			'on-ajax-simple-done',
			function(p_args)
			{
				self.log.info('GET SIMPLE DONE', p_args);
			}
		);
	};

	this.onElementsEvents = function()
	{

	};

	CjsBaseClass.call(self, $, objname, options);
};

MyNameSpace.Triggers = new MyNameSpace.Ttriggers
(
	window.cjsbaseclass_jquery,
	'triggers',
	{
		'debug'       : CJS_DEBUG_MODE_1,
		'highlighted' : 'auto'
	}
);