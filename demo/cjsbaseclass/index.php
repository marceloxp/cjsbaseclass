<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>UmsLib</title>
	<script type="text/javascript" src="../../dist/cjsbaseclass.js" data-jquery-exclusive="false" data-silent-host="www.production-site.com"></script>
	<script type="text/javascript" src="jquery.visible.js"></script>
	<script type="text/javascript" src="other.js"></script>
	<script type="text/javascript" src="ajax-test.js"></script>
	<script type="text/javascript" src="triggers.js"></script>
	<script type="text/javascript" src="main.js"></script>
	<style> body { background-color: gray; } </style>
</head>
<body>
	<div id="message">Open developer console.</div>
	<hr/>
	<form name="frmTest" id="frmTest" action="#">
		<input type="text" name="frName" id="frName" value="Person Name" placeholder="">
		<br/>
		<input type="checkbox" name="frCheck" value="check-1">check-1
		<input type="checkbox" name="frCheck" value="check-2" checked>check-2
		<input type="checkbox" name="frCheck" value="check-3" checked>check-3
		<input type="checkbox" name="frCheck" value="check-4">check-4
		<br/>
		<input type="radio" name="frRadio" value="radio-1">radio-1
		<input type="radio" name="frRadio" value="radio-2" checked>radio-2
		<input type="radio" name="frRadio" value="radio-3">radio-3
		<input type="radio" name="frRadio" value="radio-4">radio-4
		<br/>
		<label for="frSelect">Select</label>
		<br/>
		<select name="frSelect" id="frSelect">
			<option value="1">Option 1</option>}
			<option value="2" selected>Option 2</option>}
			<option value="3">Option 3</option>}
		</select>
		<br/>
		<label for="frText">Text Area</label>
		<br/>
		<textarea name="frText" id="frText" cols="80" rows="8">
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		</textarea>
	</form>

	<br/><br/><br/>

	<button id="ajax-autowait">Test Ajax Auto-Wait</button>

	<?php $hasjquery = true; ?>
	<?php if ($hasjquery): ?>
		<script src="jquery-2.2.2.min.js"></script>
		<script type="text/javascript">
			console.log('Global jQuery: ' + window.jQuery.fn.jquery);

			$(document).ready(function() {
				setTimeout
				(
					function()
					{
						$.ajax({
							url      : 'another.js',
							type     : 'GET',
							dataType : 'script'
						})
						.done(function()
						{
							console.log('Script Loaded');
						})
						.fail(function() {
							console.log('Script Fail');
						})
						.always(function() {
							console.log('Script Done');
						});
					},
					1500
				);
			});

			var counter = 0;
			var timer = setInterval
			(
				function()
				{
					console.log('Global jQuery: ' + window.jQuery.fn.jquery);
					counter++;
					if (counter >= 5)
					{
						clearInterval(timer);
					}
				},
				1000
			);
		</script>
	<?php endif ?>

	<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
	<br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>

	<div id="last_element">Last DIV Element</div>
</body>
</html>