var MyNameSpace = MyNameSpace || {};
MyNameSpace.Tajax = function($, objname, options)
{
	'use strict';
	var self = this;

	this.create = function()
	{
		self.events.onCreate();
	};

	this.onReady = function()
	{
		self.events.onReady();
	};

	this.start = function()
	{
		self.events.onStarted();
	};

	this.processTriggers = function()
	{
		self.onAjaxTriggers
		(
			'only-json',
			{
				'before': function()
				{
					self.log.info('AJAX TRIGGER FIRED [ONLY-JSON BEFORE]');
				},
				'done': function(p_response)
				{
					self.log.info('AJAX TRIGGER FIRED [ONLY-JSON DONE]');
				},
				'fail': function()
				{
					self.log.info('AJAX TRIGGER FIRED [ONLY-JSON FAIL]');
				},
				'always': function()
				{
					self.log.info('AJAX TRIGGER FIRED [ONLY-JSON ALWAYS]');
				},
				'exception': function()
				{
					self.log.info('AJAX TRIGGER FIRED [ONLY-JSON EXCEPTION]');
				}
			}
		);
	};

	this.onElementsEvents = function()
	{
		$(document).on
		(
			'click',
			'#ajax-autowait',
			function(e)
			{
				e.preventDefault();
				self.testAutoWaitAjax();
			}
		);
	};

	this.execute = function()
	{
		self.testAjax();
	};

	this.testAutoWaitAjax = function()
	{
		self.ajax
		(
			{
				'options':
				{
					slug     : 'yat',
					autowait : true,
					url      : 'test.json',
					type     : 'POST',
					dataType : 'json'
				}
			}
		);
	};

	this.testAjax = function()
	{
		self.ajax
		(
			{
				'options':
				{
					slug     : 'slug-name',
					url      : 'test.json',
					type     : 'POST',
					dataType : 'json'
				},
				'before': function()
				{
					self.log.info('TEST AJAX BEFORE');
				},
				'done': function(p_response)
				{
					self.log.info('TEST ON-AJAX DONE');
				},
				'fail': function()
				{
					self.log.info('TEST ON-AJAX FAIL');
				},
				'always': function()
				{
					self.log.info('TEST ON-AJAX ALWAYS');
				},
				'exception': function()
				{
					self.log.info('TEST ON-AJAX EXCEPTION');
				}
			}
		);

		self.ajax
		(
			{
				'options':
				{
					slug     : 'slug-simple',
					url      : 'test.json',
					type     : 'POST',
					dataType : 'json'
				},
				'done': function(p_response)
				{
					self.log.info('TEST ON-AJAX DONE');
				}
			}
		);

		self.ajax
		(
			{
				'options':
				{
					slug     : 'only-json',
					url      : 'test.json',
					type     : 'POST',
					dataType : 'json'
				}
			}
		);
	};

	CjsBaseClass.call(self, $, objname, options);
};

MyNameSpace.ajax = new MyNameSpace.Tajax
(
	window.cjsbaseclass_jquery,
	'ajax',
	{
		'debug'       : CJS_DEBUG_MODE_1,
		'highlighted' : 'auto'
	}
);