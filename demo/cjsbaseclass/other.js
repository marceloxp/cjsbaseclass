var MyNameSpace = MyNameSpace || {};
MyNameSpace.Tother = function($, objname, options)
{
	'use strict';
	var self = this;

	this.create = function()
	{
		self.counter = 0;
		self.events.onCreate();
	};

	this.onReady = function()
	{
		self.log.info('Plugin [jQuery Visible] loaded:', $('#message').visible() );
		self.events.onReady();
	};

	this.start = function()
	{
		self.events.onStarted();
	};

	this.processTriggers = function()
	{

	};

	this.onElementsEvents = function()
	{

	};

	this.execute = function()
	{
		self.utils.waitScrollTop();

		self.log.print(self.options);
		self.timer = setInterval
		(
			function()
			{
				self.log.print('Local jquery: ' + $.fn.jquery);
				self.counter++;
				if (self.counter >= 0)
				{
					clearInterval(self.timer);
				}
			},
			2000
		);

		setTimeout
		(
			function()
			{
				self.log.info('Plugin [jQuery Visible] loaded:', $('#message').visible() );
			},
			3000
		);

		var test_cookie = self.cookie.get('my_name');
		if (test_cookie === undefined)
		{
			self.cookie.set('my_name', 'my cookie value');
			self.log.print('My Cookie set');
		}
		else
		{
			self.log.print('My Cookie value:', test_cookie);
		}

		self.log.info( self.form.toJson('#frmTest') );
	};

	CjsBaseClass.call(self, $, objname, options);
};

MyNameSpace.other = new MyNameSpace.Tother
(
	window.cjsbaseclass_jquery,
	'my-other-js',
	{
		'debug'       : CJS_DEBUG_MODE_1,
		'highlighted' : 'auto'
	}
);