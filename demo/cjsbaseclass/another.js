//# sourceURL=another.js
/* global CjsBaseClass,CJS_DEBUG_MODE_0,CJS_DEBUG_MODE_1,CJS_DEBUG_MODE_2 */
var MyNameSpace = MyNameSpace || {};
MyNameSpace.Tanother = function($, objname, options)
{
	'use strict';
	var self = this;

	this.create = function()
	{
		self.events.onCreate();
	};

	this.onReady = function()
	{
		self.log.print('3 -> ON READY');
		self.log.print(self.name + ' is ready');
		self.waitReady('my-other-js', function(){ self.otherClassReady() } );

		self.events.onReady();
	};

	this.start = function()
	{
		self.events.onStarted();
	};

	this.processTriggers = function()
	{

	};

	this.onElementsEvents = function()
	{

	};

	this.execute = function()
	{
		// AUTO STARTED CODE ON CLASS READY AND STARTED
		self.utils.waitScrollTop
		(
			function()
			{
				self.log.print('Site on top Custom Callback');
			}
		);
	};

	this.otherClassReady = function()
	{
		self.log.alert('Class labeled "my-other-js" is ready!');
		self.log.info('Current Browser:' + self.browser);
		self.log.info(self.is.ie9 ? 'IS IE9 :(' : 'IS NOT IE9 :D');
		self.log.danger('cuidado!!!');
	};

	CjsBaseClass.call(this, $, objname, options);
};

MyNameSpace.another1 = new MyNameSpace.Tanother
(
	window.cjsbaseclass_jquery,
	'another1',
	{
		'debug'       : CJS_DEBUG_MODE_2,
		'highlighted' : 'auto',
		'another_opt' : 'custom_value'
	}
);

MyNameSpace.another2 = new MyNameSpace.Tanother
(
	window.cjsbaseclass_jquery,
	'another2',
	{
		'debug'       : CJS_DEBUG_MODE_2,
		'highlighted' : 'auto',
		'another_opt' : 'custom_value'
	}
);