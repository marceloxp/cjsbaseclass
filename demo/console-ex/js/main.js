window.ObjTConsoleEx = new TConsoleEx();
window.ObjTConsoleEx.init('TMyClass');
window.ObjTConsoleEx.setActive(true);
window.ObjTConsoleEx.setColor('yellow');
window.ObjTConsoleEx.log('Message Log', 3, {'name': 'xp', 'age': 42});
window.ObjTConsoleEx.info('Message Info', 3, {'name': 'xp', 'age': 42});
window.ObjTConsoleEx.alert('Message Alert', 3, {'name': 'xp', 'age': 42});
window.ObjTConsoleEx.danger('Message Danger', 3, {'name': 'xp', 'age': 42});
window.ObjTConsoleEx.setColor('navy');
window.ObjTConsoleEx.trace('Message Trace', 3, {'name': 'xp', 'age': 42});
window.ObjTConsoleEx.setDebug(false);
window.ObjTConsoleEx.print('Dont Print Message', 3, {'name': 'xp', 'age': 42});
window.ObjTConsoleEx.setDebug(true);
window.ObjTConsoleEx.print('Message Print', 3, {'name': 'xp', 'age': 42});