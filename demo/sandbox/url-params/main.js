console.log('main.js');

function parseQuery ( query ) {
   var Params = {};
   if ( ! query ) return Params; // return empty object
   var Pairs = query.split(/[;&]/);
   for ( var i = 0; i < Pairs.length; i++ ) {
      var KeyVal = Pairs[i].split('=');
      if ( ! KeyVal || KeyVal.length != 2 ) continue;
      var key = unescape( KeyVal[0] );
      var val = unescape( KeyVal[1] );
      val = val.replace(/\+/g, ' ');
      Params[key] = val;
   }
   return Params;
}

var scripts = document.getElementsByTagName('script');
console.log('main', scripts);
var myScript = scripts[ scripts.length - 1 ];
console.log('main', myScript);
console.log('main.src', myScript.src, myScript.src.replace(/^[^\?]+\??/,''));
var queryString = myScript.src.replace(/^[^\?]+\??/,'');
console.log('queryString', queryString);
var params = parseQuery( queryString );
console.log('params', params);

