console.log('main.js loaded');

var scripts = document.getElementsByTagName('script');
var current_script = scripts[scripts.length - 1];
var js_options = {};
for(var k = 0; k < current_script.attributes.length; k++)
{
   var attr = current_script.attributes[k];
   if (attr.name.substring(0,4) === 'data')
   {
      var data_name = attr.name.substr(5);
      var data_value = attr.value;
      js_options[data_name] = data_value;
   }
}
console.log('main.js dataset', js_options);